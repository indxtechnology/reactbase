import React, {Component} from 'react';
import './Header.css';

export default class Header extends Component {

    constructor() {
        super();
        console.log(" Test ");
        this.state = {
            sampleText: "sample"
        };
    }

    componentWillMount() {
        this.setState({sampleText: " header text"});
    }

    componentWillUnount() {
        this.setState({sampleText: " header text"});
    }

    changeTitle() {
        alert("Changed called");
    }

    render() {
        return (
            <div>
                <div id="header">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xs-2 text-center">
                                <div className="pull-left" id="homebutton">
                                    <i className="btl bt-bars"/>
                                </div>
                            </div>
                            <div className="col-xs-8 text-center">{this.state.sampleText}</div>
                            <div className="col-xs-2 text-center">
                                <div className="pull-right"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
