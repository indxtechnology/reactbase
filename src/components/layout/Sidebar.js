import React, {Component} from 'react';
import './Sidebar.css';

export default class Sidebar extends Component {

    constructor() {
        super();
        console.log(" Test ");
        this.state = {
            sampleText: "sample"
        };
    }

    componentWillMount() {
        this.setState({sampleText: " header text"});
    }

    componentWillUnount() {
        this.setState({sampleText: " header text"});
    }

    changeTitle() {
        alert("Changed called");
    }

    render() {
        return (
            <div id="sidebar">

                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12 text-center">
                            <div className="pull-left" id="homebutton"></div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
