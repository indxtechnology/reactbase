var React = require("react");

var Countdown = React.createClass({
    getInitialState: function() {
        window
            .Bus
            .subscribe("update-event", this.updateTitleText);
        var state = {
            titleText: false,
            aniStyle: "animated bounceIn"

        };
        return state;
    },
    updateTitleText: function(data) {
        this.setState({titleText: data.text});
    },
    changeAni() {
        var newState = {};

        if (this.state.titleText) {
            newState.aniStyle = "animated zoomIn";
            newState.titleText = false;
        } else {
            newState.aniStyle = "animated bounceIn";
            newState.titleText = true;
        }

        this.setState(newState);
    },
    render: function() {
        return (
            <p className={this.state.aniStyle} onClick={this.changeAni}>
                This is countdown counter {this.state.titleText}
            </p>
        );
    }
});
module.exports = Countdown;
