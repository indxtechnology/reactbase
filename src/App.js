import React, {Component} from 'react';
import Header from './components/layout/Header';
import './App.css'
import Countdown from './components/Countdown';
import dataStore from './stores/Datastore';

import Velocity from 'velocity-animate';

export default class App extends Component {

    constructor() {
        super();
        console.log("in constructor");
        this.state = {
            sampleText: "sample",
            employees: dataStore.getAll()
        };
    }

    componentWillMount() {
        dataStore.on("title-changed", () => {
            console.log("Data Changed");
            this.setState({
                employees: dataStore.getAll()
            });
        })
    }

    componentDidMount() {

        var node = this.refs.block;

        Velocity(node, {
            scale: 1
        }, 5000); // Velocity

    }

    render() {
        const {employees} = this.state;
        return (
            <div>
                <Header ref="heading"></Header>
                <Countdown ref="counter"></Countdown>
                <h1 ref="block" className="head">VOLA {employees.length}</h1>

            </div>
        );
    }
}
