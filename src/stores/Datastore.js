import {EventEmitter} from "events";
import Dispatcher from "../Dispatcher";

class Datastore extends EventEmitter {
    constructor() {
        super();
        this.employees = [
            {
                id: 1,
                fName: "James"
            }, {
                id: 2,
                fName: "Bond"
            }
        ];
    }

    getAll() {
        return this.employees;
    }

    updateTitle(text) {
        this.employees[0].name = text;
        this.emit("title-changed");
    }

    handleActions(action) {
        console.log(action.type);
    }

}

const dataStore = new Datastore();
window.dataStore = dataStore;
window.dispatcher = Dispatcher;
Dispatcher.register(dataStore.handleActions.bind(dataStore));

export default dataStore;
